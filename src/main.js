import Vue from 'vue';
import App from './App.vue';
import vuetify from './plugins/vuetify';
import VueTextareaAutosize from 'vue-textarea-autosize';
import firebase from 'firebase/app';
import 'firebase/firestore';

Vue.use(VueTextareaAutosize);

Vue.config.productionTip = false;

firebase.initializeApp({
  apiKey: process.env.FIREBASE_API_KEY,
  authDomain: 'vue-calendar-a11dc.firebaseapp.com',
  databaseURL: 'https://vue-calendar-a11dc.firebaseio.com',
  projectId: 'vue-calendar-a11dc',
  storageBucket: 'vue-calendar-a11dc.appspot.com',
  messagingSenderId: process.env.FIREBASE_SENDER_ID,
  appId: process.env.FIREBASE_APP_ID
});

export const db = firebase.firestore();

new Vue({
  vuetify,
  render: h => h(App)
}).$mount('#app');
