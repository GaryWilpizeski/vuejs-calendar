# vue-calendar

## Details
Quick demo calendar build using VueJS + Vuetify for the front end and Firebase on the back end. Loads in calendar events from Firebase, and UI allows you to add new events, edit existing events, or delete events. Can view calendar in day, 4-day, week, or month views, and focus on the current day at a press of a button.  Based on <a href="https://www.youtube.com/watch?v=2NOsjTT1b_k" target="_blank">this tutorial</a> by <a href="https://www.traversymedia.com/" target="_blank">Brad Traversy</a>.

### <a href="https://modest-shirley-246759.netlify.com/" target="_blank">View Calendar Here</a>

## Project setup
```
yarn install
```

## Deploy
[![Netlify Status](https://api.netlify.com/api/v1/badges/4a18f6bc-107d-4efc-985f-74fa9dbe8702/deploy-status)](https://app.netlify.com/sites/modest-shirley-246759/deploys)

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
